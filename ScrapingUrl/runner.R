# 1. check Carwling URL via Scrape
paths_allowed(paths = c("https://www.one2car.com/รถ-สำหรับ-ขาย"))

# 2. Target Web Page & inspect parameters CSS Elements 
o2c_url <- "https://www.one2car.com/รถ-สำหรับ-ขาย?page_number=1"

# 3. Get XML from target Base URL to objects
htmlread <- read_html(o2c_url) 

# 4. Get title (.listing__title)
o2c_titles <-
  o2c_url %>%
  read_html() %>%
  html_node(".listing__title") %>% 
  html_text() %>% 
  str_replace_all("\n", "") 

# 5. Get price
o2c_prices <-
  o2c_url %>% 
  read_html() %>% 
  html_node(".listing__price") %>% 
  html_text() %>%
  str_remove(" บาท") 

# 6. Get Spec
o2c_details <-
  o2c_url %>% 
  read_html() %>% 
  html_node(".listing__specs") %>% 
  html_text() %>% 
  str_replace_all("\n", "") %>% 
  trimws(which = "both") %>% 
  str_replace_all("                ", ", ") 

# 7. Pages to scrape
n_pages <- 1700
o2c_pages <- paste0("https://www.one2car.com/รถ-สำหรับ-ขาย?page_number=", 1:n_pages) #Create pages vector
o2c_listings <- data.frame() #Create empty data frame to store data after a loop finishes

# Run R CMD "as_tibble(o2c_pages)" check n pages have 1700 ?

# 8. Run For loop get data on pages
for (i in 1:length(o2c_pages)) {
  
  url <- o2c_pages[i]
  print(paste0("Getting data from page ", i, " of ",  length(o2c_pages)))
  htmlpage <- tryCatch(read_html(url), error = function(e) "HTTP 502 error") #If receive error then return "HTTP 502 error"
  
  if (htmlpage[1] == "HTTP 502 error"||length(htmlpage$node) == 0) {
    next
    #If 502 error found or cannot get anything from the node then skip to next
  } else {
    #Get title
    temp_title <-
      htmlpage %>% 
      html_nodes(".listing__title") %>%
      #We change from html_node() to html_nodes() to get the element from all nodes that have listing__title name on the web page
    html_text() %>% 
      str_replace_all("\n", "") %>% 
      trimws(which = "both")
    #Get price
    temp_price <-
      htmlpage %>% 
      html_nodes(".listing__price") %>% 
      html_text() %>%
      str_remove(" บาท")
    #Get specs
    temp_detail <-
      htmlpage %>% 
      html_nodes(".listing__specs") %>% 
      html_text() %>% 
      str_replace_all("\n", "") %>% 
      trimws(which = "both") %>% 
      str_replace_all("                ", ", ")
    #Combine all things into data frame
    o2c_combined <- data.frame(
      title = temp_title,
      price = temp_price,
      detail = temp_detail
    )
    #Store data to data frame that had been created
    o2c_listings <- rbind(o2c_listings, o2c_combined)
    Sys.sleep(0.5) #Give server some room
    print(paste0("Done ", i, " of ", length(o2c_pages)))
  }
}

#9. Save data
write.csv(o2c_listings, "/car_2_hand_dataframe.csv", row.name = FALSE)


#10. Cleaning Data
o2c_cleaning_price <- o2c_listings
o2c_cleaning_price$price[o2c$price == "ติดต่อผู้ขาย"] <- NA
o2c_cleaning_price$price <- str_remove_all(o2c$price, ",") #Remove comma
o2c_cleaning_price$price <- as.numeric(o2c$price)
o2c_cleaning_price <-
  o2c_cleaning_price %>%
  mutate_if(is.factor, as.character) #Change columns that are defined as factor to character

#11. Extract value in title column
o2c_extract_value <-
  o2c %>%
  mutate(temp = str_extract(title, "\\d{4}\\s\\w{1,}-?\\w{1,}\\s\\w{1,}-?\\w{1,}\\s\\w{1,}")) %>%
  separate(temp, c("year", "brand", "name_suba", "name_subb"), sep = " ", remove = F) %>% 
  mutate(engine_size = str_extract(title, "\\d{1}\\.\\d{1}"),
         year_model =  str_extract(title, "\\(.*\\)")) %>%
  mutate(year_model = str_remove_all(year_model, "ปี|\\(|\\)"),
         mileage = str_extract(detail, ".*กม")) %>%
  mutate(mileage = ifelse(mileage == "- กม", NA, mileage)) %>% 
  mutate(mileage = str_remove(mileage, "กม"))

#12. Merge two columns into one (type this in Source panel)
o2c_extract_value <-
  o2c_extract_value %>%
  unite("model", c("name_suba", "name_subb"), sep = "_") %>%
  mutate(model = str_remove(model, "(_\\d|NA_|_NA)"))

#13. Clean "Mazda 2 and 3" When wrong String extraction
o2c_extract_value$model[grepl("Mazda 2", o2c_extract_value$title)] <- "Mazda 2"
o2c_extract_value$model[grepl("Mazda 3", o2c_extract_value$title)] <- "Mazda 3"
o2c_extract_value$brand[grepl("Mazda", o2c_extract_value$model)] <- "Mazda"

#14. Save Clean Data 
write.csv(o2c_extract_value, "o2c_cleaned.csv")

#14.1 Filter rows with out NA
o2c_cleaned <- read.csv("o2c_cleaned.csv", header = T, stringsAsFactors = F)
o2c_cleaned <-
  o2c_cleaned %>% 
  filter(!is.na(brand)) %>%
  filter(!is.na(price)) #Get only rows with no NA

#15. Time to Visualize
#Refactor brand by max price
price_levels<-
  o2c_cleaned %>% 
  group_by(brand) %>% 
  summarise(max_price = max(price)) %>% 
  arrange(-max_price)
price_levels <- price_levels$brand
o2c_cleaned$brand <- factor(o2c_cleaned$brand, levels = price_levels)

#Use point chart to display price range by brand=
ggplot(data = o2c_cleaned, aes(x = reorder(brand, +price), y = price, col = brand)) +
  geom_point(size = 1.5, alpha = 0.3) +
  theme_bw() +
  coord_flip() +
  scale_y_continuous(labels = comma, breaks = seq(0, max(o2c_cleaned$price), 1000000)) +
  theme(legend.position = "none",
        axis.title.x = element_text(face = "bold"),
        axis.title.y = element_text(face = "bold"),
        axis.text.y = element_text(size = 6),
        axis.text.x = element_text(size = 7),
        panel.grid.minor.x = element_blank()) + labs(x = "Brand", y = "Price")

#Count by brand
o2c_by_brand <-
  o2c_cleaned %>% 
  count(brand, sort = TRUE) %>% 
  head(10)

#Plot count by car brand
o2c_by_brand %>%
  mutate(brand = reorder(brand, n)) %>% 
  ggplot(., aes(x = brand, y = n)) +
  geom_bar(stat = "identity", fill = "pink", width = 0.5) +
  coord_flip() +
  theme_minimal() +
  theme(panel.grid.major.y = element_blank(),
        axis.title = element_text(face = "bold"),
        axis.text.y = element_text(face = "italic"),
        axis.text.x = element_text(face = "italic")) +
  labs(y = "Number of listings", x = "Brand")

#Most listings by brand+model
o2c_most_listing_by_brand <-
  o2c_cleaned %>%
  inner_join(o2c_most_listing_by_brand, by = "brand") %>% #Select only brands of interest from o2c_n created
earlier                               
group_by(brand, model) %>% 
  summarise(n = n()) %>% 
  top_n(5) #Select top 5 models from top 10 n brands
#Plot
ggplot(data = o2c_n2, aes(x = reorder(model, +n), y = n, fill = brand)) +
  geom_bar(stat = "identity", width = 0.5) +
  coord_flip() +
  facet_wrap(~ brand, scales = "free_y", ncol = 3) +
  theme_minimal() +
  theme(axis.text.y = element_text(size = 9),
        legend.position = "none",
        strip.text = element_text(face = "bold"),
        axis.title = element_text(face = "bold"),
        axis.text = element_text(face = "italic"),
        panel.grid.major.y = element_blank(),
        panel.grid.minor.x = element_blank()) +
  labs(x = "Number of listings (by brand)", y = "Model")

#Price Range between Toyoto & Honda
ggplot(data = o2c_cleaned[o2c_cleaned$brand %in% c("honda", "toyota"), ],
       aes(x = model, y = price, col = brand)) +
  geom_point(alpha = 0.5, size = 1.5) +
  scale_y_continuous(labels = comma) +
  coord_flip() +
  facet_wrap(~ brand, scales = "free_y") +
  theme_minimal() +
  theme(axis.text.y = element_text(size = 7),
        legend.position = "none",
        strip.text = element_text(face = "bold"),
        axis.title = element_text(face = "bold"),
        axis.text = element_text(face = "italic"),
        panel.grid.major.y = element_blank(),
        panel.grid.minor.x = element_blank()) +
  labs(x = "Price", y = "Model")
